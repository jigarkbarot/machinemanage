import { View, Text } from 'react-native'
import React from 'react'
import { useAppSelector } from '../../redux/hooks'

const HomeScreen = () => {
  useAppSelector(state => {
    console.log(state)
  })
  return (
    <View>
      <Text>HomeScreen</Text>
    </View>
  )
}

export default HomeScreen