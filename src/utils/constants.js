import {Platform, Dimensions, StatusBar} from 'react-native';

const OS = Platform.OS;
const {width, height} = Dimensions.get('window');

export const isIPhoneX = () => {
    return OS === 'ios' && !Platform.isPad && !Platform.isTV && height >= 812;
  };
  
  export const checkNotch = () => {
    if (OS === 'ios') {
      return isIPhoneX();
    } else {
      return Boolean(StatusBar.currentHeight) && StatusBar.currentHeight > 24;
    }
  };